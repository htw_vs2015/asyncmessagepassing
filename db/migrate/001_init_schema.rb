# Datenbankmigration zur Definition der Tabellen
class InitSchema < ActiveRecord::Migration
  def change
    create_table :nodes do |t|
      t.string :name, :null => false, :unique => true
      t.string :uri, :null => false, :unique => true
      t.integer :status, :null => false, :default => 0
      t.datetime :updated_at, :null => false
    end

    create_table :incoming_messages do |t|
      t.string :msg_id, :null => false
      t.string :request_id, :default => nil
      t.string :src_name, :null => false
      t.string :msg_str, :null => false
    end

    create_table :outgoing_messages do |t|
      t.string :msg_id, :null => false
      t.string :request_id, :default => nil
      t.string :dst_name, :null => false
      t.string :msg_str, :null => false
    end
  end
end