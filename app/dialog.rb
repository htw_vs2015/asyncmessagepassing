require_relative 'deps'

# Fehlerklassen fuer den Dialog
module DialogError
  # Fehler bei der Verbindung mit einem Dienst
  class ConnectionError < StandardError
  end

  #Fehler in der Bedienerlogik
  class LogicError < StandardError
  end
end

# Dialog-Klasse zum Testen des Systems
class Dialog
  include TerminalHelpers
  attr_reader :connected_node
  DEFAULT_MANAGER_URI='druby://localhost:25565'.freeze
  MSG_AGENTNAME='Agentname'
  MENUE_MAIN="Menue:\n0) Starte Agenten\n1) Starte Reserveknoten\n2) Zeige Online Knoten\n3) Nachricht senden\n4) Knoten migrieren\n5) Agenten stoppen\n6) Neues Verzeichnis angeben\n7) Beenden".freeze
  MSG_CONNECTED_LOCAL='Aktuell mit lokalem Knoten verbunden: '.freeze
  MSG_CONNECTED_REMOTE='Aktuell mit remote Knoten verbunden: '.freeze
  MSG_NOT_CONNECTED='Aktuell mit keinem Knoten verbunden'.freeze
  MSG_ALREADY_CONNECTED='Es läuft bereits ein lokaler Knoten!'.freeze
  MSG_NO_AGENTDIR='Das Agentenverzeichnis ist nicht erreichbar!'.freeze
  MSG_INVALID_URI='Die eingegebene URI ist ungültig!'.freeze
  MSG_AGENT_NOT_REACHABLE='Der Zielknoten ist nicht erreichbar!'.freeze
  LINE='-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#'.freeze

  # Konstruktor
  def initialize(manager_uri=DEFAULT_MANAGER_URI, logger=LocalLogger.new)
    @connected_node=nil
    @manager_uri=manager_uri
    @connected_to_directory=false
    @logger=logger
    @threads=[]
    @online_agent_cache={}
  end

  # Menue anzeigen
  def menu
    @threads << Thread.new do
      while true
        begin
          if connected_to_local?
            puts (MSG_CONNECTED_LOCAL + @connected_node.to_s).colorize(:green) if connected_to_local?
          else
            puts (MSG_CONNECTED_REMOTE + @connected_node.to_s).colorize(:green) if connected?
          end
          puts MENUE_MAIN
          input=ask_for_input('Option: ')
          puts LINE
          case input
            when '0' then
              start_agent
            when '1' then
              start_agent(false)
            when '2' then
              show_online_nodes
            when '3' then
              send_message
            when '4' then
              migrate_to_node
            when '5' then
              stop_node
            when '6' then
              set_manager_uri
            when '7' then
              exit_dialog
              break
            else
              puts 'Ungueltige Eingabe!'
          end
          puts LINE
        rescue StandardError => error
          #puts error.backtrace
          print_colored("#{error.class.to_s} : #{error.message}", :red)
        end
      end
    end
    @threads.each {|thread| thread.join}
  end

  private
  # Prueft ob aktuell ein Knoten oder Agent verbunden ist
  def connected?
    not @connected_node.nil?
  end

  # Prueft ob der verbundene Agent hier lokal ist oder remote
  def connected_to_local?
    connected? && (URI(@connected_node.uri).host == get_current_host)
  end

  # Agent Starten
  def start_agent(active=true)
    check_bool(!connected?, MSG_ALREADY_CONNECTED, DialogError::LogicError)
    agentname = ask_for_input(MSG_AGENTNAME)
    begin
      @connected_node=MessageAgent.new(agentname, 'druby://' + get_current_host + ':' + get_free_port.to_s, @manager_uri, active)
    @connected_node.start
    rescue Errno::ECONNREFUSED, DRb::DRbConnError
      @connected_to_directory=false
      @connected_node=nil
      raise DialogError::ConnectionError.new(MSG_NO_AGENTDIR)
    end
  end

  #Bestimmten Agenten beenden
  def stop_node
    check_bool((connected?), MSG_NOT_CONNECTED, DialogError::LogicError)
    @connected_node.stop
    @connected_node=nil
  end

  #Auf entfernten Agent mit Dialog connecten
  def connect_to_node
    check_bool(!connected?, MSG_ALREADY_CONNECTED, DialogError::LogicError)
    agent_name=ask_for_input(MSG_AGENTNAME)
    begin
      @connected_node=DRbObject.new_with_uri(get_uri_from_name(agent_name))
    rescue Errno::ECONNREFUSED, DRb::DRbConnError
      raise DialogError::ConnectionError.new(MSG_AGENT_NOT_REACHABLE)
    end

  end

  #Alle Sendebereiten Agenten anzeigen
  def show_online_nodes
    begin
      @manager=DRbObject.new_with_uri(@manager_uri) if @node_dir.nil?
      nodes=@manager.who_is_online
      print_message_agents(nodes)
      print_inactive_nodes(nodes)
    rescue Errno::ECONNREFUSED, DRb::DRbConnError
      @connected_to_directory=false
      raise DialogError::ConnectionError.new(MSG_NO_AGENTDIR)
    end
  end

  # Eingabe einer neuen URI fuer den Verzeichnisdienst, wenn diese von der Standardeinstellung abweicht
  def set_manager_uri
    begin
      new_uri = ask_for_input('Neue Manager-URL')
      raise DialogError::LogicError.new(MSG_INVALID_URI) unless valid_uri?(new_uri)
      new_agentdir=DRbObject.new_with_uri(new_uri)
      @online_agent_cache=new_agentdir.who_is_online
      #Wenn die vorherige Zeile keine Exception wirft ist das Verzeichnis erreichbar und es wird gewechselt
      @manager_uri=new_uri
      @manager=new_agentdir
      @connected_to_directory=true
      if connected?
        @connected_node.agent_dir_uri=new_uri
      end
    rescue Errno::ECONNREFUSED, DRb::DRbConnError
      raise DialogError::ConnectionError.new(MSG_NO_AGENTDIR)
    end
  end

  #Nachricht senden
  def send_message
    check_bool(connected?, MSG_NOT_CONNECTED, DialogError::LogicError)
    target=ask_for_input('Zielknoten: ')
    msg=ask_for_input('Nachricht: ')
    @connected_node.send_message_async(target, msg)
  end

  #Knoten migrieren
  def migrate_to_node
    check_bool(connected?, MSG_NOT_CONNECTED, DialogError::LogicError)
    target_name=ask_for_input('Name des Zielknotens: ')
    target_uri=get_uri_from_name(target_name)
    if target_uri.nil?
      raise DialogError::LogicError.new(MSG_AGENT_NOT_REACHABLE)
    end
    @connected_node.migrate_to_uri(target_uri)
  end

  #Alle Agenten beenden und aufräumen
  def exit_dialog
    stop_node unless @connected_node.nil?
  end

  #Gibt eine formatierte Liste alles Nachrichtenagenten auf der Standardausgabe aus
  def print_message_agents(nodes)
    first=true
    nodes.each do |key, value|
      if value[:status]==1
        print_colored('Agenten:', :blue) if first
        puts 'Name: '.colorize(:blue) + key + ' URI: '.colorize(:blue) + value[:uri]
        first=false
      end
    end
    print_colored('Keine Agenten Online!', :blue) if first
  end

  #Gibt eine Formatierte Liste aller Reserveknoten auf der Standardausgabe aus
  def print_inactive_nodes(nodes)
    first=true
    nodes.each do |key, value|
      unless value[:status]==1
        print_colored('Reserveknoten:', :red) if first
        puts 'Name: '.colorize(:red) + key + ' URI: '.colorize(:red) + value[:uri]
        first=false
      end
    end
    print_colored('Keine Reserveknoten verfügbar!', :red) if first
  end

  # URI zu einem Name herausfinden
  # Direkte Anfrage am Verzeichnisdienst
  def get_uri_from_name(agent_name)
    begin
      if @node_dir.nil?
        @node_dir=DRbObject.new_with_uri(@manager_uri)
      end
      data=@node_dir.who_is(agent_name)
      if data.nil?
        raise DialogError::ConnectionError.new('Der Agent ist nicht Registriert')
      end
      return data[:uri]
    rescue Errno::ECONNREFUSED, DRb::DRbConnError
      raise DialogError::ConnectionError.new(MSG_NO_AGENTDIR)
    end
  end
end

dialog=Dialog.new
dialog.menu
DRb.thread.join if dialog.connected_to_local?
