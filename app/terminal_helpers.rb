# Modul mit Hilfsmethoden fuer den Dialog, Bunte Ausgabe und Eingabepruefungen
module TerminalHelpers
  #Prueft eine Bedingung und wirft eine Exception wenn die Bedingung nicht erfuellt ist
  def check_bool(boolean, error_message, error_class=StandardError)
    raise error_class.new(error_message) unless boolean
  end

  #List eine Zeile von der Standardeingabe
  def ask_for_input(prompt)
    puts prompt
    gets.chomp
  end

  #Schreibt eine Ausgabe mit bestimmter Farbe auf die Standardausgabe
  def print_colored(msg, color=nil)
    if color.nil?
      puts msg
    else
      output=msg.colorize(color)
      puts output
    end
  end

  #Prüft ob eine URI auch gültig ist und zumindest einen Host identifiziert
  def valid_uri?(uri_str)
    uri=URI(uri_str)
    (uri.host.nil? || uri.host.empty?) ? false : true
  end

  #Gibt den Hostname des aktuellen Rechners zurück
  def get_current_host
    Socket.gethostname
  end

  # Gibt einen zufälligen freien Clientport zurück
  def get_free_port
    socket = Socket.new(:INET, :STREAM, 0)
    socket.bind(Addrinfo.tcp('127.0.0.1', 0))
    port = socket.local_address.ip_port
    socket.close
    port
  end
end