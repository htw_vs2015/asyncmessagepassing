require 'active_record'
require 'sqlite3'

# Hilfklasse zur Datenbankschicht
module DatabaseHelper
  # Stellt eine Verbindung zur Datenbank her und erstellt diese wenn noetig
  def self.establish_connection(unique_name)
    project_root = get_project_root
    connection_details = YAML::load(File.open(project_root + '/../config/database.yml'))
    connection_details['database'] += (unique_name + '.sqlite')
    new_database = (not File.exists?(project_root+'/'+connection_details['database']))
    unless ActiveRecord::Base.connected?
      ActiveRecord::Base.establish_connection(connection_details)
      create_database(connection_details) if new_database or ActiveRecord::Migrator.needs_migration?(ActiveRecord::Base.connection)
    end
  end

  private
  # Erstellt eine Datenbank zu gegebenen ConnectionDetails
  def self.create_database(connection_details)
    admin_connection = connection_details.merge({'schema_search_path' => 'public'})
    ActiveRecord::Base.establish_connection(admin_connection)
    silence_stream(STDOUT) do
      ActiveRecord::Migrator.migrate("../db/migrate/")
    end
  end

  # Gibt das Root-Verzeichnis des Projektes zurueck
  def self.get_project_root
    File.dirname(File.absolute_path(__FILE__))
  end
end