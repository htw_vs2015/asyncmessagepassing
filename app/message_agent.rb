require_relative 'deps'

# Klasse zur Abbildung eines Knotens mit optionaler Agentenfunktionalitaet
class MessageAgent
  attr_reader :active_agent, :node_dir, :uri, :name
  attr_accessor :logger

  # Setter fuer die URI zum Verzeichnisdienst
  # Kann Remote vom Dialog aufgerufen werden
  def agent_dir_uri=(new_uri)
    begin
      @manager.unregister(@name)
    rescue Errno::ECONNREFUSED, DRb::DRbConnError
      #Nichts tun, wenn das Verzeichnis offline ist muss/kann auch nicht abgemeldet werden
    end
    @manager=DRbObject.new_with_uri(new_uri)
    if @active_agent
      status=Agent::AgentStatus::ACTIVE
    else
      status=Agent::AgentStatus::INACTIVE
    end
    @manager.register(@name, @uri, status)
  end

  # Konstruktor
  # wenn active=true  --> Agent
  # wenn active=false --> Reserveknoten
  def initialize(name, uri, manager_uri, active=true, logger=LocalLogger.new)
    @name=name
    @active_agent=active
    @manager_uri=manager_uri
    @uri=uri
    @agent_table_cache={}
    @logger=logger
    @send_threads=[]
    @receive_threads=[]
  end

  # Knoten / Agent im Netzwerk propagieren und Datenbank-Verbindung herstellen
  def start
    DRb.start_service(self.uri, self)
    @manager=DRbObject.new_with_uri(@manager_uri)
    DatabaseHelper.establish_connection(@name)
    go_online
  end

  # Registerierung am Verzeichnis
  def go_online
    if @active_agent
      status=Agent::AgentStatus::ACTIVE
    else
      status=Agent::AgentStatus::INACTIVE
    end
    @manager.register(@name, @uri, status)
    @agent_table_cache=@manager.who_is_online
    process_buffered_messages if @active_agent
  end

  # Beim Verzeichnis abmelden und Dienst vom Netzwerk trennen
  def stop
    go_offline
    ActiveRecord::Base.remove_connection
    DRb.stop_service
  end

  # Nachricht verarbeiten
  # wird Remote aufgerufen
  def process_message(message)
    return Agent::AgentStatus::INACTIVE unless active_agent
    return Agent::AgentStatus::WRONG_DESTINATION if message.dst_name != @name
    new_msg=IncomingMessage.new_instance(msg_id: message.msg_id, request_id: message.request_id, src_name: message.src_name, msg_string: message.message_string)
    new_msg.save!
    work_with_msg_async(new_msg)
    Agent::AgentStatus::ACTIVE
  end

  def handle_alive_status(status_msg)
    logger.log("Statusupdate: Knoten #{status_msg.agent_id_of_change} : #{human_readable_life_status(status_msg.status)}")
    case status_msg.status
      when Agent::REGISTERED
        # URI-Zuordnungscache erneuern wenn eine URI in der Statusnachricht ist
        initialize_cache(status_msg.agent_id_of_change)
        @agent_table_cache[status_msg.agent_id_of_change][:uri]=status_msg.agent_uri_of_change unless status_msg.agent_uri_of_change.nil?
        # Nachrichten an Agenten erneut Senden wenn noch im Puffer
        resend_messages_for_agent(status_msg.agent_id_of_change)
      when Agent::UNREGISTERED
        @agent_table_cache.delete(status_msg.agent_id_of_change)
      else
        raise 'Ungueltiger Status in der Statusnachricht!'
    end
  end

  # Prueft ob der angegebene name registriert ist oder nicht
  def check_registered(name)
    if (target_uri=get_agent_uri(name)).nil?
      raise Agent::AgentNotFoundError.new("Agent #{name} ist nicht registriert")
    end
    target_uri
  end

  # Nachricht aus String asynchron an ziel senden
  # @param target_name: Zielagent
  # @param message_string: Nachricht
  # Wird Remote aufgerufen
  def send_message_async(target_name, message_string, request_id=nil)
    raise Agent::AgentNotActiveError.new('Dieser Knoten ist kein Agent.') unless @active_agent
    message=Message.new(message_string, target_name, @name, request_id)
    buffer_outgoing(target_name, message)
    @send_threads << Thread.new {
      send_message_sync(message)
    }
  end

  # Stoesst eine Migration zu einer neuen URI eines Reserveknotens an
  # Wird Remote aufgerufen
  # @param uri - Ziel-URI
  def migrate_to_uri(uri)
    target_node=DRbObject.new_with_uri(uri)
    @active_agent=false
    go_offline
    new_name=target_node.name
    outgoing_msgs=OutgoingMessage.all.to_a
    incoming_msgs=IncomingMessage.all.to_a
    begin
      # Blockierender, Synchroner Aufruf
      target_node.initialize_from_migration(@name, outgoing_msgs, incoming_msgs, @agent_table_cache)
      if URI(uri).host!=Socket.gethostname
        OutgoingMessage.destroy_all
        IncomingMessage.destroy_all
      end
      @name=new_name
      @manager.register(new_name, @uri, 0)
      ActiveRecord::Base.remove_connection
      DatabaseHelper.establish_connection(@name)
    rescue => e
      @logger.log("Exception beim Umzug: #{e.message}", :error)
      @logger.log('Umzug nicht erfolgreich, versuchen Sie es später nochmal', :warn)
      @active_agent=true
      go_online
    end

  end

  # Initialisiert einen Reserveknoten und schaltet ihn zum Agenten um, wird bei der Migration genutzt
  # Wird Remote aufgerufen
  # @param name - Name des umziehenden Agenten
  # @param sendbuffer - Serialisierter Sendepuffer des umziehenden Agenten
  # @param recievebuffer - Serialisierter Empfangspuffer des umziehenden Agenten
  # @param adress_cache - Namenstabelle des umziehenden Agenten
  def initialize_from_migration(name, sendbuffer, recievebuffer, adress_cache)
    raise Agent::AgentActiveError.new('Dieser Agent ist kein Reserveknoten') if @active_agent
    ActiveRecord::Base.remove_connection
    @manager.unregister(@name)
    @name=name
    @agent_table_cache=adress_cache
    DatabaseHelper.establish_connection(@name)
    recover_outgoing(sendbuffer)
    recover_incoming(recievebuffer)
    @active_agent=true
    @manager.register(@name, @uri, Agent::AgentStatus::ACTIVE)
  end

  def to_s
    @name
  end

  #--------------- PRIVATE ACCESS ------------ #
  private
  # Sendet alle Nachrichten die fuer einen Agenten im Sendepuffer stehen erneut
  def resend_messages_for_agent(name)
    buffer_for_agent = OutgoingMessage.where(dst_name: name)
    buffer_for_agent.each do |msg|
      destination=msg.dst_name
      message_string=msg.msg_str
      buffer_for_agent.where(msg_id: msg.msg_id).destroy_all
      send_message_async(destination, message_string)
    end
  end

  # Wandelt den Status in einen lesbaren String um (on / offline)
  def human_readable_life_status(alive_status)
    if alive_status==0
      "Offline"
    else
      "Online"
    end
  end

  # Synchrone Sendemethode -- Immer in eigenem Thread nutzen
  def send_message_sync(message, try_again=true)
    begin
      target_uri=check_registered(message.dst_name)
      target_agent=DRbObject.new_with_uri(target_uri)
      response_code = target_agent.process_message(message)
      case response_code
        when Agent::AgentStatus::ACTIVE
          if message.request_id.nil?
            @logger.log("Nachricht mit ID: #{message.msg_id} an #{message.dst_name} zugestellt!", :success)
          else
            @logger.log("Reply mit ID: #{message.msg_id} an #{message.dst_name} zugestellt!", :success)
          end
          OutgoingMessage.where(dst_name: message.dst_name).where(msg_id: message.msg_id).destroy_all
        when Agent::AgentStatus::WRONG_DESTINATION
          @agent_table_cache.delete(status_msg.agent_id_of_change)
          send_message_sync(message, false) if try_again #Einfach nochmal senden und Adresse anfordern
          raise Agent::AgentNotActiveError.new('Der Zielknoten hat eine andere Adresse angemeldet')
        else
          raise Agent::AgentNotActiveError.new('Der Zielknoten ist kein Agent')
      end
    rescue DRb::DRbConnError, Agent::AgentNotFoundError, Agent::AgentNotActiveError => connerror
      logger.log(connerror.to_s, :warn)
      if (not @manager.nil?) and (not connerror.is_a?(Agent::AgentNotActiveError))
        begin
          @manager.report_unresponsive_agent(message.dst_name)
        rescue => e
          #puts e
          #puts e.message
          #puts e.backtrace
          logger.log('Fehler bei der Kommunikation mit dem Verzeichnisdienst!', :warn)
        end
      end
    rescue => e
      logger.log('Programmierfehler: ' + e.to_s + 'AN:' + e.backtrace.to_s + 'Fehlertyp: ' + e.class.to_s, :error)
    end
  end

  # Abmeldung beim Verzeichnis
  def go_offline
    @manager.unregister(@name)
    @send_threads.each do |t|
      t.exit
    end
    @receive_threads.each do |t|
      t.exit
    end
  end

  # Alle noch gepufferten Ein- und Ausgehenden Nachrichten abarbeiten
  def process_buffered_messages
    Thread.new do
      IncomingMessage.all.each do |msg|
        work_with_msg_async(msg)
      end
      @agent_table_cache.each_key do |key|
        resend_messages_for_agent(key)
      end
    end
  end

  # Bearbeitet eine Nachricht asynchron und sendet ggf. ein Reply
  def work_with_msg_async(message)
    @receive_threads << Thread.new do
      begin
        @logger.log('Nachricht Empfangen - ' + message.to_s)
        sleep(5)
        send_message_async(message.src_name, "Antwort auf Nachricht #{message.msg_id}", message.msg_id) if (message.request_id.nil?)
        IncomingMessage.find_by_msg_id(message.msg_id).destroy!
      rescue => e
        puts e
      end
    end
  end

  # Nachricht in den Sendepuffer einfügen
  def buffer_outgoing(agentname, msg)
    OutgoingMessage.create!(msg_id: msg.msg_id, dst_name: agentname, msg_str: msg.message_string, request_id: msg.request_id)
  end

  # Fuegt eine ganze Map von Nachrichten in den Sendepuffer ein
  # Wird bei der Initialisierung vom Umzug verwendet
  def recover_outgoing(outgoing_msgs)
    outgoing_msgs.each do |msg|
      msg.save!
    end
  end


  # Fuegt eine ganze Map von Nachrichten in den Empfangspuffer ein
  # Wird bei der Initialisierung vom Umzug verwendet
  def recover_incoming(incoming_msgs)
    incoming_msgs.each do |msg|
      msg.save!
    end
  end

  # Eintrag im Namescache leer vorbelegen
  def initialize_cache(agentname)
    unless agentname.nil?
      @agent_table_cache[agentname]={} if @agent_table_cache[agentname].nil?
    end
  end

  # Loest einen Agentenname zu einer URI auf,
  # dabei wird erst im Cache, dann am Verzeichnis geprueft
  def get_agent_uri(agent_name)
    result = @agent_table_cache[agent_name]
    if result.nil?
      result = @manager.who_is(agent_name)
      @agent_table_cache[agent_name]=result
    end
    if result.nil?
      nil
    else
      result[:uri]
    end
  end
end
