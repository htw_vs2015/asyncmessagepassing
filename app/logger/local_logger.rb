# KLasse fuer lokales Logging mit Loglevel und Timestamps
class LocalLogger
  LOG_COLORS={:warn => :yellow, :info => :blue, :error => :red, :success => :green}

  # Konstruktor
  def initialize(tabs=3)
    @tab=""
    tabs.times do
      @tab<<"\t"
    end
  end

  # Eine Nachricht mit bestimmtem loglevel (prioritaet) loggen
  def log(msg,prio=:info)
    puts "#{@tab}#{get_time}\t#{prio.to_s.upcase!}: #{msg}".colorize(:color => LOG_COLORS[prio])
  end

  private
  # Gibt die Systemzeit im Format HH:MM:SS zurueck
  def get_time
    Time.new.strftime("%H:%M:%S")
  end

end