require_relative 'deps'
# Klasse, die den Knoten- / Agentenverzeichnisdienst implementiert
class NodeDirectory
  attr_accessor :logger

  # Konstruktor mit optionaler angabe eines logger-Objekts
  def initialize(logger=LocalLogger.new)
    @logger=logger
  end

  # Dienst starten, Datenbankverbindung herstellen
  # und Objekt im Netzwerk verfuegbar machen
  def start
    DRb.start_service(MANAGER_URI, self)
    DatabaseHelper.establish_connection('directory')
    validate_cache
    @logger.log("Agentenverzeichnis bereit und Online!", :success)
  end


  # Meldet einen Knoten / Agenten am System an
  # Wird Remote aufgerufen
  def register(agent_name, agent_uri, agent_status)
    current_entry = Node.find_by_name(agent_name)
    if current_entry.nil?
      Node.create!(name: agent_name, uri: agent_uri, status: agent_status, updated_at: DateTime.now)
    else
      current_entry.uri = agent_uri
      current_entry.status = agent_status
      current_entry.updated_at=DateTime.now
      current_entry.save!
    end
    inform_agents_about_change(agent_name, Agent::REGISTERED)
  end

  # Meldet einen Agenten am System ab
  # Wird Remote aufgerufen
  def unregister(agentname)
    found_node = Node.find_by_name(agentname)
    found_node.destroy! unless found_node.nil?
    inform_agents_about_change(agentname, Agent::UNREGISTERED)
  end

  # Gibt eine Map alles Knoten / Agenten mit URI und Status zurueck
  # Wird Remote aufgerufen
  def who_is_online
    agents={}
    Node.all.each do |node|
      agents[node.name]={uri: node.uri, status: node.status}
    end
    agents
  end

  # Gibt URI und Status zu einem Name zurueck
  # Wird Remote aufgerufen
  def who_is(name)
    found_node = Node.find_by_name(name)
    return nil if found_node.nil?
    {uri: found_node.uri, status: found_node.status}
  end

  def report_unresponsive_agent(name)
    unregister(name)
  end

  private
  # Fuehrt eine Pruefung der lokalen Namenstabelle im Verzeichnis nach einem Absturz bzw. neustart durch
  # Dadurch nimmt das System nach einem kurzen Ausfall schnellsmoeglich wieder alle Dienste auf
  def validate_cache
    unregistered_nodes=[]
    online_nodes=[]
    query_threads=[]
    Node.all.each do |node|
      query_threads << Thread.new {
        begin
          remote_node = DRbObject.new_with_uri(node.uri)
          remote_name = remote_node.name
          if node.name != remote_name
            node.name=remote_name
            node.save!
          end
          online_nodes << node.name
          @logger.log("Name von Knoten #{remote_name} validiert!")
        rescue DRb::DRbConnError
          @logger.log("Knoten #{node.name} ist offline!")
          unregistered_nodes << node.name
          node.destroy!
        end
      }
      query_threads.each { |t| t.join }
    end
  end

  # Informiert alle angemeldeten Agenten ueber Statusaenderungen im Gesamtsystem
  # also An- und Abmeldungen sowie Ausfaelle
  def inform_agents_about_change(agent_name, status)
    @logger.log("Anmeldung von: #{agent_name}") if status==Agent::REGISTERED
    @logger.log("Abmeldung von #{agent_name}") if status==Agent::OFFLINE
    Node.all.each do |node|
      current_name = node.name
      current_uri = node.uri
      if current_name != agent_name
        Thread.new {
          begin
            current_agent = DRbObject.new_with_uri(current_uri)
            msg=Agent::StatusChangeMessage.new(current_name, agent_name, status) if status==Agent::UNREGISTERED
            msg=Agent::StatusChangeMessage.new(current_name, agent_name, status, who_is(agent_name)[:uri]) if status==Agent::REGISTERED
            current_agent.handle_alive_status(msg)
          rescue DRb::DRbConnError => e
            @logger.log("Verbindungsfehler: #{e.message}", :warn)
            unregister(current_name)
          rescue => e
            @logger.log("#{e.message} AN: #{e.backtrace.to_s}", :error)
          end
        }
      end
    end
  end
end