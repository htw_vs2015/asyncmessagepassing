module Agent
  UNREGISTERED=0
  REGISTERED=1
  OFFLINE=2

  # Klasse zur Darstellung von Statusaenderungen bei Knoten und Agenten
  class StatusChangeMessage
    attr_reader :agent_id_of_change, :agent_uri_of_change, :status
    def initialize(target_agent,agent_id_of_change,status, agent_uri_of_change=nil)
      @message_target=target_agent
      @agent_id_of_change=agent_id_of_change
      @agent_uri_of_change=agent_uri_of_change
      @status=status
    end
  end

  # Exception wenn ein Agent nicht gefunden wurde
  class AgentNotFoundError < StandardError
  end

  # Exception wenn ein Ziel-Agent gerade nicht aktiv ist (also nur Reserve)
  class AgentNotActiveError < StandardError
  end

  # Exception wenn beim Umzug das Ziel kein Reserveknoten ist
  class AgentActiveError < StandardError
  end


  # Verschiedene Status von Agenten
  # ACTIVE --> Aktiv als Nachrichtenagent
  # INACTIVE --> Nur als Reserveknoten
  # WRONG_DESTINATION --> Fuer diese Aktion das falsche Ziel (Zielname ist falsch belegt)
  module AgentStatus
    ACTIVE=1
    INACTIVE=0
    WRONG_DESTINATION=2
  end
end