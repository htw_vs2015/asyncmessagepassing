require_relative '../deps'
# ORM-Klasse fuer Nachrichten im Empfangspuffer
# Bildet Tabelle 'incoming_messages' ab
class IncomingMessage < ActiveRecord::Base

  def self.new_instance(msg_id: ,src_name: , msg_string:, request_id:)
    msg=IncomingMessage.new
    msg[:msg_id]=msg_id
    msg[:src_name]=src_name
    msg[:msg_str]=msg_string
    msg[:request_id]=request_id
    msg
  end

  def to_s
      "ID: #{msg_id}, SRC: #{src_name}, MSG: #{msg_str}"
  end
end