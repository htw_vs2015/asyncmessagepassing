require_relative '../deps'
# ORM-Klasse fuer Nachrichten im Empfangspuffer
# Bildet Tabelle 'outgoing_messages' ab
class OutgoingMessage < ActiveRecord::Base
  def self.new_instance(msg_id: ,dst_name: , msg_string:, request_id: nil)
    msg=OutgoingMessage.new
    msg[:msg_id]=msg_id
    msg[:dst_name]=dst_name
    msg[:msg_str]=msg_string
    msg[:request_id]=request_id
    msg
  end

    def to_s
      if request_id.nil?
        "ID: #{msg_id}, DST: #{dst_name}, MSG: #{msg_str}"
      else
        "ID: #{msg_id}, RESPONSE_TO: #{request_id}, DST: #{dst_name}, MSG: #{msg_str}"
      end
    end
end