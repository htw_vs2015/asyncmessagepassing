# Nachrichtenklasse zum Austausch zwischen Agenten
class Message
  attr_reader :msg_id, :message_string, :dst_name, :src_name, :request_id
  @@id=1

  # Konstruktor
  def initialize(message,target_name,src_name,request_id=nil)
    @msg_id=@@id
    @@id+=1
    @message_string=message
    @dst_name=target_name
    @src_name=src_name
    @request_id=request_id
  end

  # toString()-Methode
  def to_s
    "ID: #{@msg_id}, REPLY_TO: #{@request_id || 'X'} SRC: #{src_name}, DST: #{@dst_name}, MSG: #{@message_string}"
  end
end
